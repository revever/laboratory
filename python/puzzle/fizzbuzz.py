# https://en.wikipedia.org/wiki/Fizz_buzz
# gotcha: l[f:] returns empty instead of IndexOutOfArray error

for i in range(1, 20):
    print("fizz"[i % 3 * 4:] + "buzz"[i % 5 * 5:] or i)
