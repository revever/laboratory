import urllib.request
import os
import time
import logging

url = "http://localhost:5000/"

logging.basicConfig(
    format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG)

while True:
    try:
        folder = time.strftime("%Y%m%d")
        os.makedirs(folder, exist_ok=True)

        filename = time.strftime("%Y%m%d_%H%M%S.html")
        logging.info(f"snapshotting {url}...")
        urllib.request.urlretrieve(url, os.path.join(folder, filename))
        logging.info(f"{filename} archived.")
    except Exception as e:
        logging.exception(e)

    # better crash if the sleep fails - otherwise the loop will act like DoS attack
    time.sleep(300)
