import os
import re

# pattern ex: 20240101
p = re.compile("[\d]{8}")

count = 0
max_count = 1000


def add_date(dir, file):
    m = p.search(file)
    if not m:
        print(f"KO!!! {file}")
        return False

    if m.start() == 0:
        #print(f"OK: {file}")
        return False

    nFile = m.group()[1:9] + "_" + file[0 : m.start()] + file[m.end() :]
    print(f"{file} -> {nFile}")
    # os.rename(os.path.join(d, file), os.path.join(d, nFile))
    return True


for d, dirList, fileList in os.walk(
    r"\\NasA\users\family_doc\04-LOYER_IMMO\Roitelets\photo"
):
    print(f"----- {d}")
    for file in fileList:
        if file == "Thumbs.db":
            continue
        if count < max_count:
            if add_date(d, file):
                count += 1
