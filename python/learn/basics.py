# positional arguments & keyword arguments
def arguments(*positional, **kwargs):
    print("positional:", positional)
    print("keywords:", kwargs)


arguments(1, 2, 3)
arguments(one=1, two=2, three=3)  # name with =


# lambda (arguments before :)
l = [2, 1, 3]
print(sorted(l, key = lambda x : -x))


# yield -> generator -> iterator
def yield_func():
    yield 1
    yield 2


gen = yield_func()
print(next(gen))
print(next(gen))
print(next(gen))

# zip -> transpose into tuples
langs = ['Java', 'Python', 'C++']
versions = (17, 3, 20)
z = zip(langs, versions)
print(next(z))
