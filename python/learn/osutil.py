import os

# --- where a module is located
print(os.__file__)

# X: list recursively the files with walk
# ✓: list the files with walk
for root, folders, files in os.walk("../util"):
    path = root.split(os.sep)
    print((len(path) - 1) * '\t', "+", os.path.basename(root))
    for file in files:
        print(len(path) * '\t', file)

# the recursive calls are implemented inside walk. Walk just yield them
pyfiles = [os.path.join(root, file) for root, dirs, files in os.walk("../util") for file in files if file.endswith(".py")]
print(pyfiles)
