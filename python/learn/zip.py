import zipfile

# read a zip
with zipfile.ZipFile("test.zip", "r") as zip:
    for fileInfo in zip.filelist:
        print(fileInfo.filename)

# create a zip
with zipfile.ZipFile("test.zip", "w") as zip:
    zip.write("test.txt")

