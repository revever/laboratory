import subprocess

# Popen() is not-blocking while run() is blocking: https://docs.python.org/3/library/subprocess.html

# this will print to the console (note: args should be separated as list)
subprocess.run(["java", "-version"])

# this will capture the output
result = subprocess.run(["java", "-version"], capture_output=True)  # stdout/stderr -> subprocess.PIPE
print(result.stderr)  # result.stderr is binary string (b'...')
print(result.stderr.decode("utf-8"))
