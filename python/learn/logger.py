import logging
import time

logging.basicConfig(filename='my.log', format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG)
logging.info("this is an example log @%s", time.strftime("%Y-%m-%d %H:%M:%S"))
