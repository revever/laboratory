# Abbreviations
a: array,  l: length,  p: position

# Best Practices
- be careful with the inputs (args validation is by default the first thing to do)
- start by studying simple examples
- coding while looking at these simple concrete examples
- 