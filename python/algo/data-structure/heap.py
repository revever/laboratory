# readings: https://fr.wikipedia.org/wiki/Tas_(informatique)
# Ideal for tasks with priority, ex: Dijkstra's algorithm.

# Specially, the binary heap can be implemented with an array.
# Fibonacci heap is actually a set of trees and have better performance (see below)

"""
The insert/deletion process is similar to 'bubble up' (set & repair)
-------
Operation	find-max	delete-max	insert	increase-key	meld
==
Binary[8]	Θ(1)	Θ(log n)	O(log n)	O(log n)	Θ(n)
Leftist	Θ(1)	Θ(log n)	Θ(log n)	O(log n)	Θ(log n)
Binomial[8][9]	Θ(1)	Θ(log n)	Θ(1)[b]	Θ(log n)	O(log n)[c]
Fibonacci[8][10]	Θ(1)	O(log n)[b]	Θ(1)	Θ(1)[b]	Θ(1)
Pairing[11]	Θ(1)	O(log n)[b]	Θ(1)	o(log n)[b][d]	Θ(1)
Brodal[14][e]	Θ(1)	O(log n)	Θ(1)	Θ(1)	Θ(1)
Rank-pairing[16]	Θ(1)	O(log n)[b]	Θ(1)	Θ(1)[b]	Θ(1)
Strict Fibonacci[17]	Θ(1)	O(log n)	Θ(1)	Θ(1)	Θ(1)
2–3 heap[18]	O(log n)	O(log n)[b]	O(log n)[b]	Θ(1)	?
"""

import heapq

q = [1]
heapq.heappush(q, 2)  # note: the heappush is not defined with self as first parameter. it's module function.
