# https://leetcode.com/problems/letter-combinations-of-a-phone-number/
# 之后有延申


m = ["", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"]
md = {str(i): m[i] for i in range(10)}


# DFS
def phone_dfs(s):
    if not s:
        return []

    rst, q = [], []

    def bt(p):
        if len(q) == len(s):  # do not check 'q>=len(s)' as at this moment we have len(q)==len(s)
            rst.append(''.join(q))
            return

        for c in md[s[p]]:
            q.append(c)
            bt(p + 1)
            q.pop()

    bt(0)
    return rst


print(phone_dfs("23"))


# 注意到每个键的可能值是无重复的，我们可以用for来实现
def phone_for(s: str):
    if not s:
        return []

    rst = ['']
    for c in s:
        rst = [r + l for r in rst for l in md[c]]  # 2 for are needed to reduce
    return rst


print(phone_for("23"))

# 以上恰是product函数(multiple for)
import itertools


def phone_product(s: str):
    if not s:
        return []
    return [''.join(t) for t in itertools.product(*(md[c] for c in s))]  # note the use of *


print(phone_product("23"))

##################################
"""
给定一个字典(list)，需求是随着用户按键，需要实时提示可能的单词。例如按23，则需要提示after, bet, better等单词。
"""

d = ['better', 'box', 'test', 'after', 'bet']

# pre-processing
l2n = {ord(c): ord(k) for k, v in md.items() for c in v}
w2n = [(w.translate(l2n), w) for w in d]
w2n.sort()

import bisect

def candidates_bisect(s: str):
    if not s:
        return []
    lo = bisect.bisect_left(w2n, (s,))
    hi = bisect.bisect_left(w2n, (s[:-1] + chr(ord(s[-1])+1),))
    return sorted([v[1] for v in w2n[lo:hi]])

# Another approach is trie with defaultdict

print(candidates_bisect('23'))
