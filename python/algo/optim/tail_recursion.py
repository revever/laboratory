""" 
All recursive algos risk to have stack-overflow issue. A tail-recusive algo
can always be replaced by an iterative procedure.
"""


# recursive
def fabonacci(n):
    return fabonacci(n - 1) + fabonacci(n - 2) if n > 1 else 1


print(fabonacci(5))


# iterative
def fab_iterative(n):
    a, b = 1, 1
    while n > 0:
        a, b = b, a + b
        n -= 1
    return a


print(fab_iterative(100))

# functools
# reminder: decorator is basically a wrapper
# idea: DP (dynamic-programming)
import functools


@functools.cache
def fab_functools(n):
    return fab_functools(n - 1) + fab_functools(n - 2) if n > 1 else 1


print(fab_functools(100))
