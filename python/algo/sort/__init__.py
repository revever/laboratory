"""
https://en.wikipedia.org/wiki/Sorting_algorithm#Bubble_sort

For in-place recursive algorithms, the space complexity is not constant but
O(log n) due to stack space.

Merge-sort is highly parallelizable. Therefore its idea is naturally suitable 
for big-data.
"""