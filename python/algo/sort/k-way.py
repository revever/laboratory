# 313: https://leetcode.com/problems/super-ugly-number/

import heapq

# v1 issue: duplicates numbers may be pushed into heap
def nth_ugly_v1(primes, n):
    q = [1]
    for _ in range(1, n):
        u = heapq.heappop(q)
        for prime in primes:
            heapq.heappush(q, prime * u)
    return q[0]


# v2 (v1_fix) issue: out-of-memory when n is huge
def nth_ugly_v2(primes, n):
    q, l = [1], []
    while len(l) < n:
        u = heapq.heappop(q)
        if l and u == l[-1]:
            continue

        l.append(u)
        for prime in primes:
            heapq.heappush(q, prime * u)
    return l[-1]

# v3: K-way merge with heap (O(n*logk))
"""
equation: next-ugly = ugly * prime (1)  or prime * ugly (2)
reciprocal vision:
    1) the number of ugly numbers increases -> hard to manage
    2) the number of primes are given -> k-way merging

duplicate: it's possible that we push duplicates in heap (iff n1 * p1 = n2 * p2)
"""
def nth_ugly_v3(primes, n):
    r, q = [1], [(p, p, 0) for p in primes]
    for _ in range(n - 1):
        r.append(q[0][0])
        while r[-1] == q[0][0]:
            _, p, i = heapq.heappop(q)
            heapq.heappush(q, (p * r[i + 1], p, i + 1))
    return r[-1]

# KISS
# There are 2 lists in this problem: the list of ugly numbers and the list of primes.
def nth_ugly(primes, n):
    r, q = [1], [(p, p, 0) for p in primes]
    while len(r) < n:
        num, p, i = heapq.heappop(q)
        if num != r[-1]:
            r.append(num)
        heapq.heappush(q, (p * r[i + 1], p, i + 1))
    return r[-1]

print(nth_ugly([2,7,13,19], 12))

