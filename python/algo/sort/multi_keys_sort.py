# 1996: https://leetcode.com/problems/the-number-of-weak-characters-in-the-game/

# builtin sort: print([s for s in dir(__builtins__) if 'sort' in s])

"""
1. asc sort or desc sort? we are searching for weak characters. 最自然的选择是指针所指处即为目标 -> desc order
2. 为了简化同attack间的无用比较，我们对defense按升序排列
"""


def solution(properties) -> int:
    count, maxDef = 0, 0
    properties.sort(key=lambda x: (-x[0], x[1]))

    for p in properties:
        if p[1] < maxDef:
            count += 1
        else:
            maxDef = p[1]

    return count


print(solution([[7, 7], [1, 2], [9, 7], [7, 3], [3, 10], [9, 8], [8, 10], [4, 3], [1, 5], [1, 5]]))
"""
[
    [9,7], 
    [9,8],
    [8,10],
    [7,3],  // Weak
    [7,7],  // Weak
    [4,3],  // Weak
    [3,10], 
    [1,2],  // Weak
    [1,5],  // Weak
    [1,5]   // Weak
]
"""
