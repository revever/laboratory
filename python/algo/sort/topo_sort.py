# leetcode: 207. Course Schedule

from collections import defaultdict, deque

def topology_sort(numCourses: int, prerequisites: list[list[int]]) -> bool:
    # init degrees
    deps = defaultdict(list)
    degrees = {i:0 for i in range(numCourses)}
    for cor, dep in prerequisites:
        deps[dep].append(cor)
        degrees[cor] += 1

    # deque is linked-list, we use it to store free nodes (indegree = 0)
    indeps = deque([k for k,v in degrees.items() if v == 0])
    while indeps:            
        node = indeps.popleft()
        numCourses -= 1
        for cor in deps[node]:
            degrees[cor] -= 1
            if degrees[cor] == 0:
                indeps.append(cor)

    return numCourses == 0

