# https://en.wikipedia.org/wiki/Quicksort

import random

# data = random.sample(range(100), 10)  # no duplicate
data = [random.randint(1, 20) for _ in range(10)]
print(data)


# ------------------------------------------------------
def quick_sort(array):
    if not array:
        return array

    pivot = array[0]
    left = [x for x in array if x < pivot]
    middle = [x for x in array if x == pivot]
    right = [x for x in array if x > pivot]
    # recursive call
    return quick_sort(left) + middle + quick_sort(right)


print(quick_sort(data))


# ------------------------------------------------------
"""
structure: def quicksort:
    check
    pivot = ...
    qs(sub_array1)
    qs(sub_array2)
    
"""


def bidirectional_qs(array, start=0, end=None):
    if not array:
        return  # inplace: return nothing

    if end == None:  # once a big bug here with 'if not end' --> end = 0/None both captured!
        end = len(array) - 1

    if start >= end:
        return

    pivot = bidirectional_partition(array, start, end)
    bidirectional_qs(array, start, pivot - 1)
    bidirectional_qs(array, pivot, end)


"""
Rule 0 of Divide-Conquer: the problem must be reduced in any case
*1: array[lo] <= pivot will violate rule 0 (ex: [5, 2] with pivot = 5)
*2: no need to check 'lo <= hi'. The basic fact is that all numbers before lo are <= pivot 
and all numbers after hi is >= pivot --> when lo >= hi, naturally the loop quit
*3: !!! basically, if lo == hi, then return lo+1; else return lo --> hi + 1
"""


def bidirectional_partition(array, start, end):
    lo, hi = start, end
    pivot = array[start]  # once a big bug here with 'pivot = array[0]'
    while True:
        while array[lo] < pivot: lo += 1  # (*1)(*2)
        while array[hi] > pivot: hi -= 1
        if lo >= hi:
            return hi + 1  # (*3)
        array[lo], array[hi] = array[hi], array[lo]
        lo, hi = lo + 1, hi - 1


bidirectional_qs(data)
print(data)
