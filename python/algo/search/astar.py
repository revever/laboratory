"""
https://en.wikipedia.org/wiki/A*_search_algorithm

1. Path-finding is not just about tree, but about search. So we need visited = set() or .visited property.
2. We may need to reconstruct the path, so we need incoming = dict() or .incoming.
"""

""" 
DFS: depth-first, BFS: breadth-first, A*: best-first.
The word 'first' implies a queue. In DFS, the order is naturally guaranteed by the call stack. For BFS, the order is 
given by FIFO list. For A*, it's the duty of heap (priority-queue).
"""

import heapq


def AStar(start, target):
    q = [(f(start), start)]  # THE queue
    while q:
        dist, node = heapq.heappop(q)
        if not node.visited:  # avoid search loop
            # check if node is solution
            node.visited = True
            for neighbour in node.neighbors:
                node.dist = dist + 1
                neighbour.incoming = node
                heapq.heappush(q, (f(neighbour), neighbour))


def f(node):
    return g(node) + heuristic(node)


def g(node):
    return node.dist


def heuristic(node):
    # 0 is the simplest, nevertheless an admissible heuristic value
    return 0
