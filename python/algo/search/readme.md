# 最简单图形：BST
___
The big picture for the following example:
<pre>
    1
   /\  
  2  3
 / \
4   5
</pre>

**DFS**:
注：广义的树只有root-first和root-last，但是对于BST还有InOrder.
- Inorder [Left, Root, Right] : 4 2 5 1 3 
- root-fiRst (pReorder) [Root, Left, Right] : 1 2 4 5 3 
- root-laST (poSTorder) [Left, Right, Root] : 4 5 2 3 1

**BFS**: also known as level-order: 1 2 3 4 5

**A***: best-first


The word 'first' implies a queue. In DFS, the order is naturally guaranteed by the call stack (FILO). For BFS, the order is 
given by FIFO list. For A*, it's the duty of heap (priority-queue).


# 图搜索
---
广义的图，譬如五角星⛤，没有左右节点，没有root，也没有level，因此没有所谓的DFS/BFS，自然更没有in/pre/post order的区别。

```
build_adjs
seens
q
while(q):
    poll
    if(seen):
        continue
    
    // update & prune
    update seens
    if all seen or target reached
        return
        
    // enqueue
    for adj in adjs:
        if not seen:
            enqueue
```

为了避免重复访问，通常使用一个visited的集合来保存访问过的节点。在某些情况下，我们还需要记忆正在访问的路径，此时可以用一个states数组，0表示未访问，1表示该路径正在被遍历，2表示该路径已经遍历完成...一个例子是
环的探测。<1表示该路径正在被遍历>，揭示了虽然最终遍历全空间，但是在任意时刻都只有一条路径。



### A*
f=g+h  (g:起始点到n的实际距离, h:n到目标点的估算距离)
* h=0: 即为dijkstra，效率最低
* h<h*： 效率居中
* h=h*：效率最高
* h>h*:效率更高，但是未必找到最优解。例如某个节点判断h为无穷大，则实际上该节点被排除在外了，此时找到的路径未必是最优的。

# Backtracking
Backtracking与狭义搜索的区别：
- 对象未必是图（例如sudoku）
- 搜索是**由已知向未知**搜索，当前是否答案通过已搜索过的节点即可知晓。而BT是**由未知向已知**收敛，当前是否答案必须在探索完子空间后才知晓。
例如在cycle_detection中，当前节点是否存在环并不能从已经搜索过的节点中推理出来，而必须从其子节点中推理出来。