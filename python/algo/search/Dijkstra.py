# readings: https://fr.wikipedia.org/wiki/Algorithme_de_Dijkstra
#           https://fr.wikipedia.org/wiki/Algorithme_de_Bellman-Ford
#           https://fr.wikipedia.org/wiki/Algorithme_de_Floyd-Warshall
# Dijkstra is BFS or a A* with h=0.
# Leecode 743: https://leetcode.com/problems/network-delay-time/

"""
用归纳法可以证明该算法。如同冰封现象，同一个节点可以被冰封多次。注意，q中存放的不是节点，而是时间线（不同路径冰封节点的时间）。

对于有负边的图，不可以通过抬高（所有边加一个正常数）的方式来处理。因为不同的路径会包含不同数目的边，所以不同的路径被抬高的总量会不一样。这也可以
很容易通过构造一个反例来证明。
"""

# Complexity with binary-array heap. There are 2 operations in this algo that will trigger queue's shifting:
# pop: "while q: time, node = heapq.heappop(q)"：注意不是执行V次，而是E次
# push: "for v, w in adj[node]: heapq.heappush()"： 一共执行E次（其实从全局上看，push和pop的数量应该是守恒的）。
# 故复杂度为Elog(E)或者Elog(V)。（因为E <= V(V-1)，故logE < logV^2 = 2logV）

from collections import defaultdict
import heapq


def networkDelayTime(times: list[list[int]], n: int, k: int) -> int:
    closed, q, adj = {}, [(0, k)], defaultdict(list)  # or adj = [[]]*n (behind dict is array)
    for u, v, w in times:
        adj[u].append((v, w))

    while q:
        time, node = heapq.heappop(q)
        if node not in closed:  # not frozen yet
            closed[node] = time
            if len(closed) == n:
                return time  # pruning. all nodes are frozen now
            for v, w in adj[node]:
                if v not in closed:  # worth as it avoids a log(V) operation
                    heapq.heappush(q, (time + w, v))
    return -1  # default case (it should be the first line of the method)


print(networkDelayTime([[2, 1, 1], [2, 3, 1], [3, 4, 1]], 4, 2))

"""
v1:
print(max(visited.values()) if len(visited) == n or -1)
"""
