# DP
# as all recursive algorithm, there is always a depth parameter in the main function

# https://leetcode.com/problems/house-robber/

from functools import cache


def rob(nums):
    @cache
    def dp(start):
        return max(nums[start] + dp(start + 2), dp(start + 1)) if start < len(nums) else 0

    return dp(0)


def rob_iter(nums):
    r1, r2 = 0, 0
    for n in nums:
        r1, r2 = r2, max(r1 + n, r2)
    return r2   # initially r1 points to index -2 and r2 points to -1.


print(rob([2, 7, 9, 3, 1]))
print(rob_iter([2, 7, 9, 3, 1]))


# --------------------
def rob_v1(nums: list[int]) -> int:
    cache = {}

    def dp(nums, start):
        if start in cache:
            return cache[start]

        r = 0
        if start < len(nums):
            r = max(nums[start] + dp(nums, start + 2), dp(nums, start + 1))
        cache[start] = r
        return r

    return dp(nums, 0)
