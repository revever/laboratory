"""
有1欧，50分，20分，10分，5分和1分的硬币，请问一共有多少种方式可以凑够1欧（各种硬币数目不限）。
---
边界条件是最容易被忽略的但也是最不应该被忽略的，因为边界是限定主体的关键。
"""

from functools import cache

units = [100, 50, 20, 10, 5, 2, 1]
def coins(total):
    @cache
    def bt(sub, lv):
        # check
        if sub < 0 or lv >= len(units):
            return 0
        if sub == 0:
            return 1

        count = 0
        for i in range(sub//unit):
            count += bt(sub - i * unit, )