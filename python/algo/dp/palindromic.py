# palindromic string: https://leetcode.com/problems/longest-palindromic-substring/
# coding with ex: "babad", "bb"

"""
在v2中，（1）处可以优化。
"""

"""
v2:
辅助数组的每个元素可以记载以s[i]为中心的最长回文。这个用法更切合回文的特性。
不足之处：复杂度是o(N^2)。
总结：1)自然的初始化 2)代码化自然思路，避免过早的优化
"""


def palindromic(s):
    a = [(i, i + 1, True) for i in range(len(s))]  # (start, end-x, all-same-letter) 自然的初始化
    for i in range(1, len(s)):
        for j in range(i):
            st, end, sl = a[j]
            if end != i:  # (1)
                continue
            if st > 0 and s[st - 1] == s[i]:  # symmetric
                a[j] = (st - 1, end + 1, False)
            if sl and s[i] == s[st]:  # all same letters
                a[j] = (st, end + 1, True)
    l, p, _ = max(a, key=lambda t: t[1] - t[0])
    return s[l:p]


"""
辅助数组a的每个元素记载着以s[i]结尾的最长回文。  
问题：bananas的结果是错误的。原因在于当下最长的那个回文未必不会被其他的超过，应该保存所有结束于s[i]的回文。
"""


def palindromic_v1(s):
    a = [(1, 0, True)]  # (len, start, all-same-letter)
    for i in range(1, len(s)):
        lp, sp, ep = a[i - 1]
        if ep and s[i] == s[sp]:
            a.append((lp + 1, sp, True))
        elif sp > 0 and s[sp - 1] == s[i]:
            a.append((lp + 2, sp - 1, False))
        else:
            a.append((1, i, True))
    l, p, _ = max(a)
    return s[p:p + l]


"""
最简单的：for i in range(len(s)): find_max_with_s[i]_as_center
"""


def longest_palindrome(self, s: str) -> str:
    res = ""

    def helper(left, right):
        while left >= 0 and right < len(s) and s[left] == s[right]:
            left -= 1
            right += 1
        return s[left + 1: right]

    for i in range(len(s)):
        res = max(helper(i, i), helper(i, i + 1), res, key=len)
    return res


assert palindromic("aacabdkacaa") == "aca"
assert palindromic("ABCDCBA") == "ABCDCBA"
assert palindromic("bananas") == "anana"
assert palindromic("xxxxxxxa") == "xxxxxxx"
