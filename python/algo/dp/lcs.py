from functools import cache

"""
坑：乘数组(l*n)是指针拷贝。例如m=[[0]*2]*3，如果修改m[0][0]则会修改所有的m[*][0]。
"""


# 小技巧：数据尺寸大1用来存储初始状态。
def lcs_matrix(s1, s2):
    m, n = len(s1), len(s2)
    a = [[0 for i in range(m + 1)] for j in range(n + 1)]
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            if s1[i - 1] == s2[j - 1]:
                a[j][i] = 1 + a[j - 1][i - 1]
            else:
                a[j][i] = max(a[j - 1][i], a[j][i - 1])
    return a[n][m]


def lcs_recursive(s1, s2):
    @cache
    def dp(p1, p2):
        if p1 < 0 or p2 < 0:
            return 0

        if s1[p1] == s2[p2]:
            return dp(p1 - 1, p2 - 1) + 1
        else:
            return max(dp(p1 - 1, p2), dp(p1, p2 - 1))

    return dp(len(s1) - 1, len(s2) - 1)


# assert lcs("hello", "hell") == 4
assert lcs_matrix("ezupkr", "ubmrapg") == 2
