"""
Leetcode 300: https://leetcode.com/problems/longest-increasing-subsequence/

通常我们需要一个辅组数组l：a）l[i]要么是前i个元素的lis，b)要么是以nums[i]为最后一个元素的is(increasing seq)的长度。
设定a没有保存序列的任何信息，所以无法递归，没有意义。
"""

import bisect

# time: O(nlogn)   space: O(n^1/2)
def lis(nums):
    l = []
    for n in nums:
        if not l or l[-1] < n:
            l.append(n)
        else:
            l[bisect.bisect_left(l, n)] = n  # update minimal last element
    return len(l)


# 发现真正需要记录的是每个长度的最小尾元素
def lis_v3(nums):
    d = {}  # l : minimal-last-element
    for n in nums:
        ml = 1
        for dl, dn in d.items():
            if n > dn:
                ml = max(ml, dl + 1)
        d[ml] = n
    return max(d.keys())


# observation: 对于长度为l的序列，只需要考虑最后一个。
# 证明：假设有两个长度同为l的序列，第二个序列的最后一个元素必然小于或等于第一个序列的最后一个元素。
# !!!!!!!!!!!!! complexity: O(N^1.5)
def lis_v2(nums):
    d = {}  # length in worst-case: N^1/2
    for i in range(0, len(nums)):
        m = 1
        for l, p in d.items():
            if nums[i] > nums[p]:
                m = max(m, l + 1)
        d[m] = i
    return max(d.keys())


# 1 as initial state is more explicit
def lis_v1_bis(nums):
    l = [1]
    for i in range(1, len(nums)):
        m = 1
        for j in range(0, i):
            if nums[i] > nums[j]:
                m = max(m, l[j] + 1)
        l.append(m)
    return max(l)


def lis_v1(nums):
    if not nums:  # useless as the problem states that len(nums) > 0
        return 0

    l = []
    for i in range(len(nums)):
        m = 1
        for j in range(0, i):
            if nums[i] > nums[j]:
                m = max(m, l[j] + 1)
        l.append(m)
    return max(l)


assert lis([0, 1, 0, 3, 2, 3]) == 4, "!"
assert lis([10, 9, 2, 5, 3, 7, 101, 18]) == 4
print("tests pass!")
