DP通常都有一个辅助数组（一维或二维），用于memoriser状态。这个状态需要是一致的，例如在palindromic里面，
如果记录以s[i]为最后一个字母的最长回文是错误的，因为以s[j](j<i)结尾的回文肯定不会超过该回文，但是以s[i]
结尾的其他回文可能变得更长。

DP通常有recursive和iterative两种形式。推荐使用iterative，以避免stack-overflow。



