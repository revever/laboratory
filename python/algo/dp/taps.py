# https://leetcode.com/problems/minimum-number-of-taps-to-open-to-water-a-garden/

from functools import cache
import math

"""
Math.inf: inf + any_other_number = inf
"""


class Solution:
    def minTaps(self, n: int, ranges: list[int]) -> int:
        @cache
        def dp(garden, tap):
            if garden >= n:
                return 0
            if tap > n:
                return math.inf

            lower, upper = tap - ranges[tap], tap + ranges[tap]
            if lower <= garden <= upper:
                return min(dp(garden, tap + 1), 1 + dp(upper, tap + 1))
            else: # useless to open this tap
                return dp(garden, tap + 1)

        rst = dp(0, 0)
        return -1 if rst == math.inf else rst


print(Solution().minTaps(3, [0, 0, 0, 0]))  # -1
print(Solution().minTaps(5, [3, 4, 1, 1, 0, 0]))  # 1


class Solution_V1:
    def minTaps(self, n: int, ranges: list[int]) -> int:
        @cache
        def dp(garden, tap):  # pi: start not covered, ri: free robinet start
            if tap > n:
                return -1
            if garden >= n:
                return 0

            openCount = dp(max(garden, tap + ranges[tap]), tap + 1)
            closeCount = dp(garden, tap + 1)
            print(openCount)
            if openCount == -1 and closeCount == -1:
                return -1
            elif openCount == -1:
                return closeCount
            elif closeCount == -1:
                return openCount + 1
            else:
                return min(openCount + 1, closeCount)

        return dp(0, 0)
