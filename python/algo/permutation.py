# natural solution
# （插入算法：如果我们知道P(N)，那么P(N+1)相当于在每个P(N)的0...N位置插入第N+1个元素，即P(N+1) = (N+1)*P(N)）

# --------------------- permutation

# note: use yield from (python3.3+) to delegate sub-generator

def permutation(choices, k):
    yield from __permutation([], choices, k)


def __permutation(container: list, choices: tuple, k: int):
    for choice in choices:
        if choice in container:
            continue
        container.append(choice)
        if len(container) == k:
            yield container
        else:
            yield from __permutation(container, choices, k)
        container.pop()


it = permutation("ABC", 3)
print(next(it))
print(next(it))


# --------------------- combination
# at each level, we can only choose from the remaining letters (see dp/inventions/coins.py).

def combination(choices, k):
    __combination([], 0, choices, k)


"""
v2: in a recursive procedure (ex: bt), there are 2 basic things:
    - the definition of level
    - the stop condition
Note: the depth of the problem is k not len(choices).每一层不是choices[i]取不取的问题，而是取哪一个choices的问题（蕴含必然取）。
"""
from collections import deque


def __combination(container: deque, p: int, choices: tuple, k: int):
    if len(container) == k:
        print(container)
        return

    for i in range(p, len(choices)):
        container.append(choices[i])
        __combination(container, i + 1, choices, k)
        container.pop()


def __combination_v1(container: list, p: int, choices: tuple, k: int):
    for i in range(p, len(choices)):
        container.append(choices[i])
        if len(container) == k:
            print(container)
        else:
            __combination_v1(container, i + 1, choices, k)
        container.pop()


combination("ABC", 2)
