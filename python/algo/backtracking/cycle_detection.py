"""
A clever algorithm for cycle detection is tortoise and hare. But it works only for cycle of nodes having only
one egress, otherwise 'node.next' won't be possible. (note: the key to find loop's entry is to reset tortoise at
the beginning and then move both tortoise & hare step by step. m + n = k*C). When the nodes may have multiple egresses,
then we must exhaust each node's egresses => that's backtracking.
"""

"""
搜索算法通常有一个源点。从这个意义上看，has_loop方法可以被视为一个搜索算法。
复杂度: 得益于states的缓存作用，复杂度为O(N)
"""

# ex: https://leetcode.com/problems/course-schedule/

from collections import defaultdict, deque

# exercise: use python's dynamic attribute: node.state

# state array without visited
def dfs_cycle(numCourses: int, prerequisites: list[list[int]]) -> bool:
    # note: deps = [[]]*numCourses DOESN'T work because the []*n will copy the reference of the same []
    deps, states =  [[] for _ in range(numCourses)], [0] * numCourses  # 1: being checked, 2: certified
    for cor, dep in prerequisites:
        deps[cor].append(dep)

    # you must be able to imagine a dynamic search in the head
    def has_loop(node):
        if states[node] == 2:
            return False
        if states[node] == 1:  # run into the same node during traversal
            return True

        states[node] = 1
        for dep in deps[node]:
            if not has_loop(dep):
                return True
        states[node] = 2
        return False

    for i in range(numCourses):
        if has_loop(i):
            return False
    return True


print(dfs_cycle(2, [[1,0],[0,1]]))


# state array
def dfs_cycle_v3(numCourses: int, prerequisites: list[list[int]]) -> bool:
    deps = [[] for _ in range(numCourses)]
    for cor, dep in prerequisites:
        deps[cor].append(dep)

    states = [0] * numCourses  # 1: being checked, 2: certified

    # you must be able to imagine a dynamic search in the head
    def dfs(visited: set, node):
        if states[node] == 2:
            return True
        if states[node] == 1:  # run into the same node during traversal
            return False

        states[node] = 1
        for dep in deps[node]:
            if not dfs(visited, dep):
                return False
        states[node] = 2
        return True

    for i in range(numCourses):
        if not dfs(set(), i):
            return False
    return True


def dfs_cycle_v1(numCourses: int, prerequisites: list[list[int]]) -> bool:
    deps = defaultdict(list)
    for cor, dep in prerequisites:
        deps[cor].append(dep)

    # you must be able to imagine a dynamic search in the head
    def dfs(visited: set, node):
        if node in visited:
            return False
        
        visited.add(node)
        for dep in deps[node]:
            if not dfs(visited, dep):
                return False
        visited.remove(node)
        return True
    
    for i in range(numCourses):
        if not dfs(set(), i):
            return False
    return True


# memorize the certified nodes
def dfs_cycle_v2(numCourses: int, prerequisites: list[list[int]]) -> bool:
    deps = defaultdict(list)
    for cor, dep in prerequisites:
        deps[cor].append(dep)

    certified = [0 for _ in range(numCourses)] # [0]*numCourses

    # you must be able to imagine a dynamic search in the head
    def dfs(visited: set, node):
        if certified[node]:
            return True
        if node in visited:
            return False
        
        visited.add(node)
        for dep in deps[node]:
            if not dfs(visited, dep):
                return False
        visited.remove(node)

        certified[node] = 1
        return True
    
    for i in range(numCourses):
        if not dfs(set(), i):
            return False
    return True




