Backtracking est utilisé notamment pour résoudre des problèmes de satifaction de contraintes. Il permet de tester
systématiquement l'ensemble des affectations potentielles du problème.

Backtracking exploite les sous-espaces du problème en profondeur. A cet égard, il est un DFS spécifique qui, dans 
la plupart des cas, doit mémoriser les choix à chaque profondeur. Alors que DFS basique se contente de visiter 
les noeuds.

