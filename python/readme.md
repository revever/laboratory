# FAQ

***
* Which is the path of my env and where is the 'module'?

  `sys.path & module.__file__`


* why "can't find module X'?
  
  Check firstly if your module or your python file is named 'X'.

* need to check documentation of a variable 'x'
  
  `help(type(x))`