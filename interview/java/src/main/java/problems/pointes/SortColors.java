package problems.pointes;

import java.util.Arrays;

/**
 * https://leetcode.com/problems/sort-colors/submissions/1420796337/
 * 
 * KISS: keep it simple & stupid. 
 * There may be a solution with 3 pointers. But let's begin with 2 pointers.
 * 
 * 2 pointers:
 * while(true) {
 *     while(x): move_p1
 *     while(y): move_p2
 *     if(z): process(p1,p2)
 *     else: break
 * }
 */
public class SortColors {
    public static void main(String[] args) {
        int[] nums = new int[] {2,0,2,1,1,0};
        new SortColors().sortColors(nums);
        System.out.println(Arrays.toString(nums));
    }

    public void sortColors(int[] nums) {
        int s1 = extracted(nums, 0, 0);
        extracted(nums, 1, s1);
    }

    private int extracted(int[] nums, int target, int begin) {
        int i = begin;
        int j = nums.length - 1;
        while (true) {
            while (i < j && nums[i] == target) { // 对撞指针：relation i/j
                i++;
            }
            while (j > i && nums[j] != target) { // 对撞指针：relation i/j
                j--;
            }

            if(i >= j) {  // break or not!
                break;
            } else {
                int t = nums[i];
                nums[i] = nums[j];
                nums[j] = t;
            }
        }
        return i;
    }

}
