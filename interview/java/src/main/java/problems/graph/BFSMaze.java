package problems.graph;

/**
 * https://www.codingame.com/ide/puzzle/moves-in-maze
 * https://www.codingame.com/ide/puzzle/island-escape
 *
 * 1. use proper model, ex: Cell (KISS)
 * 2. BFS is FIFO (q.offerLast + q.pollFirst)
 * 3. !!! 1中的独立class通常是mutable的，会引入一个难以察觉的问题：不同路径会修改同一个cell的depth。这里有两个解决方案：a）只有depth优于
 *      原depth才修改 b)depth不放在Cell中，而是引入一个DepthCell的immutable类。
 */
public class BFSMaze {
}
