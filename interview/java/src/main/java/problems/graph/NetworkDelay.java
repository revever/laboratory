package problems.graph;

import java.util.*;

/**
 * https://leetcode.com/problems/network-delay-time/
 *
 * Use record & comparator to simply the code.
 *
 * map.computeIfAbsent receives a functional parameter. map.getOrDefault receives a value parameter.
 */
public class NetworkDelay {

    record DelayNode(int delay, int node) {
    }

    public static void main(String[] args) {
        System.out.println(new NetworkDelay().networkDelayTime(new int[][]{{2, 1, 1}, {2, 3, 1}, {3, 4, 1}}, 4, 2));
    }


    public int networkDelayTime(int[][] times, int n, int k) {
        Map<Integer, List<int[]>> adjs = new HashMap<>();
        for (int[] time : times) {
            adjs.computeIfAbsent(time[0], node -> new ArrayList<>()).add(time); // no striping redundant info
        }

        Set<Integer> seens = new HashSet<>();
        PriorityQueue<DelayNode> q = new PriorityQueue<>((x1, x2) -> x1.delay - x2.delay);
        q.add(new DelayNode(0, k));
        while (!q.isEmpty()) {
            DelayNode dn = q.poll();
            if (seens.contains(dn.node)) {
                continue;
            }

            seens.add(dn.node);
            if (seens.size() == n) {
                return dn.delay;
            }

            // adjs
            for (int[] adj : adjs.getOrDefault(dn.node, Collections.emptyList())) {
                if (!seens.contains(adj[1])) {
                    q.add(new DelayNode(dn.delay + adj[2], adj[1]));
                }
            }
        }
        return -1;
    }
}
