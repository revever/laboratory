package problems.graph;

import java.util.*;

/**
 * https://leetcode.com/problems/number-of-provinces/submissions/1420437642/
 * <p>
 * 对于图搜算问题，请使用队列+状态(visited或状态），而不是每次都试着发明一个新算法。
 */
public class Provinces {

    public static void main(String[] args) {
        System.out.println(new V1().findCircleNum(new int[][]{{1, 1, 0}, {1, 1, 0}, {0, 0, 1}}));
    }

    static class V1 {
        Set<Integer> visited = new HashSet<>();

        public int findCircleNum(int[][] isConnected) {
            int n = isConnected.length;
            Queue<Integer> q = new LinkedList<>();
            int count = 0;
            for (int i = 0; i < n; i++) {
                if (!visited.contains(i)) {
                    count++;
                    q.add(i);
                    travel(isConnected, q);
                }
            }
            return count;
        }

        private void travel(int[][] isConnected, Queue<Integer> q) {
            while (!q.isEmpty()) {
                int city = q.poll();

                // visited
                if (visited.contains(city)) {
                    continue;
                }
                visited.add(city);

                // prune
                if (visited.size() == isConnected.length) {
                    return;
                }

                // queue
                for (int i = 0; i < isConnected.length; i++) {
                    if (isConnected[city][i] == 1 && !visited.contains(i)) {
                        q.add(i);
                    }
                }
            }
        }
    }
}
