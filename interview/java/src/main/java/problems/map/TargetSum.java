package problems.map;

import java.util.*;

/**
 * https://leetcode.com/problems/two-sum/description/
 */
public class TargetSum {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(new V1().twoSum(new int[]{3,2,4}, 6)));
        System.out.println(Arrays.toString(new V2().twoSum(new int[]{3,2,4}, 6)));
    }
}

class V2 {
    public int[] twoSum(int[] nums, int target) {

        Map<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length; i++) {
            int n = nums[i];
            Integer pos = map.get(target - n);
            if (pos == null) {
                map.put(n, i);
            } else {
                return new int[] {i, pos};
            }
        }

        for(int i = 0; i < nums.length; i++) {
            int n = nums[i];
            int remain = target - n;
            Integer index = map.get(remain);
            if(index != null && index != i) { // #1
                return new int[] {i, index};
            }
        }
        return new int[]{0, 0};
    }
}

/**
 * #1: map is an image of nums: self-projection issue
 */
class V1 {
    public int[] twoSum(int[] nums, int target) {
        // map number:index
        Map<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length; i++) {
            map.put(nums[i], i);
        }

        for(int i = 0; i < nums.length; i++) {
            int n = nums[i];
            int remain = target - n;
            Integer index = map.get(remain);
            if(index != null && index != i) { // #1
                return new int[] {i, index};
            }
        }
        return new int[]{0, 0};
    }
}