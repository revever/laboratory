package problems.dp;

/**
 * https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/description/
 *
 * "you can buy it then immediately sell it on the same day."
 */
public class SellStock2 {
    public int maxProfit(int[] prices) {
        int profit = 0;
        for(int i=1; i<prices.length;i++) {
            if(prices[i] > prices[i-1]) {
                profit += prices[i] - prices[i-1];
            }
        }
        return profit;
    }
}
