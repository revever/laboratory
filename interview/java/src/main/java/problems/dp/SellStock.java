package problems.dp;

/**
 * https://leetcode.com/problems/best-time-to-buy-and-sell-stock/submissions/1421134362/
 *
 * - memorize the max profit
 * - today's max profit must be relative to min-price until today
 */
public class SellStock {

    public static void main(String[] args) {
        System.out.println(new SellStock().maxProfit(new int[]{7, 1, 5, 3, 6, 4}));
    }

    public int maxProfit(int[] prices) {
        int maxProfit = 0;
        int minPrice = prices[0];
        for (int i = 1; i < prices.length; i++) {
            int profit = prices[i] - minPrice;
            if (profit > maxProfit) {
                maxProfit = profit;
            }
            if (prices[i] < minPrice) {
                minPrice = prices[i];
            }
        }
        return maxProfit;
    }
}
