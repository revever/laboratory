package problems.dp;


/**
 * https://leetcode.com/problems/minimum-number-of-taps-to-open-to-water-a-garden/
 *
 * 1. infinity也是一个数值
 * 2. 转移方程可以是作用在一个range上
 */
public class TapWater {

    public static void main(String[] args) {
        System.out.println(new Solution().minTaps(5, new int[] {3,4,1,1,0,0})); // 1
    }

    static class Solution {
        public int minTaps(int n, int[] ranges) {
            int[] mins = new int[n + 1];
            for(int i = 1; i <= n; i++) {
                mins[i] = n * 2;
            }

            for(int i = 0; i <=n; i++) {
                int range = ranges[i];
                int lower = Math.max(1, i - range + 1);
                int upper = Math.min(n, i + range);
                for(int j = lower; j <= upper; j++) {
                    mins[j] = Math.min(mins[j], mins[lower - 1] + 1);
                }
            }

            return mins[n] <= n? mins[n] : -1;
        }
    }
}
