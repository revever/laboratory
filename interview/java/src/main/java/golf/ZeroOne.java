package golf;

interface ZeroOne {
    // output: 0 0 00 0000 0 000 00 0000 0 00
    static void main(String[] args) {
        solution_v2_1(args);
    }

    // b>0?"":0  --> 0 will be cast as string
    static void solution(String[] a) throws Exception {
        for (int c, i, b, f = 2; (c = System.in.read()) > 10;) // p: current suite, 0 or 1
            for (i = 7; i-- > 0; b = c >> i & 1, System.out.print(b == f ? 0 : (f > 1 ? "" : " ") + (b > 0 ? "" : 0) + "0 0"), f = b)
                ;
    }

    // !  toCharArray = getBytes
    static void solution_v3(String[] a) {
        int i, b, f = 2;
        for (int c : "CC".getBytes())
            for (i = 7; i-- > 0; b = c >> i & 1, System.out.print(b == f ? "0" : (f > 1 ? "" : " ") + ((f = b) > 0 ? "0 0" : "00 0")))
                ;
    }

    // #1  toCharArray = getBytes
    // #2  %08d to pad digit with 0. (%8s to format string with ' ', then replace all ' ' by '0')
    static void solution_v2_1(String[] a) {
        var s = "CC";
        s.chars().mapToObj(c -> s.format("%7s", Long.toBinaryString(c)).replaceAll(" ", "0").replaceAll("01", "0")).forEach(System.out::print);
    }

    static void solution_v2(String[] a) {
        //String s = new java.util.Scanner(System.in).nextLine();
        // no lambda: as lambda requires i to be final or effectively final
        int i = 0, j, b, f = 2;
        for (int c : "CC".toCharArray())
            for (j = 7; j-- > 0; b = c >> j & 1, System.out.print(b == f ? "0" : (i++ > 0 ? " " : "") + ((f = b) > 0 ? "0 0" : "00 0")))
                ;
    }

    // int a=5, b[]=new int[a]
    static void solution_v1(String[] a) {
        //String s = new java.util.Scanner(System.in).nextLine();
        String s = "C";

        int l = s.length() * 7, i = 0, j = 0, c = 0, b[]= new int[l];
        for (; i < l / 7; i++) {
            c = s.charAt(i);
            for (j = 6; j >= 0; c /= 2, j--)
                b[i * 7 + j] = c % 2;
        }

        //System.out.println(Arrays.toString(b));
        // j: the current suite, 0 or 1
        for (; c < l; c++)
            System.out.print(b[c] != j ? (c > 0 ? " " : "") + ((j = b[c]) > 0 ? "0 0" : "00 0") : "0");
    }
}
