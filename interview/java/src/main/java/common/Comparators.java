package common;

import java.util.*;

import static java.util.Comparator.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * Use the static methods of Comparator, not Comparators.
 *
 * The comparator interface is just: int compare(T o1, T o2);
 */
public class Comparators {

    // note: by default, record type is static
    record Player(String name, int rank) {
    }

    public static void main(String[] args) {
        default_sort();
        null_decorator();
    }

    /**
     * If the T is comparable (ex: Number), the most simple sort is Collections.sort.
     */
    private static void default_sort() {
        List<Integer> list = List.of(3, 1, 2); // here returns ImmutableCollection
        assertThatThrownBy(() -> Collections.sort(list))
                .isInstanceOf(UnsupportedOperationException.class);
        System.out.println(list);

        System.out.println(Collections.max(list));
        System.out.println(list.stream().max(Integer::compareTo));
        System.out.println(list.stream().mapToInt(i -> i).max());
        // frequencey
        // rotate
        // nCopies
        //...

    }

    private static void null_decorator() {
        List<Player> players = new LinkedList<>();
        players.add(new Player(null, 1));
        players.add(new Player("Tom", 2));
        players.add(new Player("John", 2));

        // null
        assertThatThrownBy(() -> players.sort(Comparator.comparing(Player::name)))
                .isInstanceOf(NullPointerException.class);

        // decorate: nullLast acts on type (Player), not on key (Player::name)
        assertThatThrownBy(() -> players.sort(nullsLast(comparing(Player::name))))
                .isInstanceOf(NullPointerException.class);

        players.sort(comparing(Player::name, nullsLast(reverseOrder())));

        // use a KEY-comparator parameter to process null keys
        players.sort(comparing(Player::name, nullsLast(naturalOrder())));
        assertThat(players.getLast().name).isNull();
        assertThat(players.getFirst().name).isEqualTo("John");
    }
}
