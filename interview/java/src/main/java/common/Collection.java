package common;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

public class Collection {

    public static void main(String[] args) {
        heap();
        primitivesArray();
        primitivesStream();
        array();
        list();
        set();
        map();
    }

    private static void primitivesArray() {
        int[] array1 = {1, 3, 4, 2};
        Integer[] array2 = {1, 3, 4, 2};
        //var array3 = {1, 3, 4, 2};   // not compile

        assertThat(Set.of(array1)).hasSize(1);   // primitive array is seen as a single object!!!!!

        assertThat(Set.of(array2)).hasSize(4);   // but wrapped array is seen as var-args

        // Arrays.stream(array1).collect(Collectors.toSet());   // not compile: primitive streams are not compatible with object
        assertThat(Arrays.stream(array1).boxed().collect(Collectors.toSet())).hasSize(4);    // boxed to wrap!!!!
    }

    private static void primitivesStream() {
        int[] array = new int[]{1, 3, 4, 2};

        IntStream stream = Arrays.stream(array); // method 1
        stream = IntStream.of(array); // method 2

        System.out.println(stream.mapToObj(String::valueOf).collect(Collectors.joining("-"))); // joining collector
    }

    private static void array() {
        List<Integer> l = List.of(3, 1, 4, 2, 3, 4);

        // can't cast Integer[] to int[], there is no auto-unwrap. BECAUSE array is object (can't cast orange as apple)
        Integer[] array = l.toArray(new Integer[0]);
        System.out.println(Arrays.toString(array));

        // use stream to get primitive array
        int[] ints = l.stream().mapToInt(Integer::intValue).toArray();
        // use Arrays.toString
        System.out.println(Arrays.toString(ints));
    }

    private static void list() {
        // of return Immutable
        List<Integer> l = List.of(3, 1, 4, 2, 3, 4);

        l.forEach(System.out::println);
    }

    private static void set() {
        List<Integer> l = List.of(3, 1, 4, 2, 3, 4);

        // indexed: LinkedHashSet
        // normally collection has a contructor which accepts another collection
        Set<Integer> ordered = new LinkedHashSet<>(l);  // simple than: l.forEach(e -> set.add(e))
        System.out.println(ordered);

        // sorted: TreeSet with red-black tree (implements SortedSet)
        Set<Integer> sorted = new TreeSet<>(l);
        System.out.println(sorted);
    }

    private static void map() {
        List<Integer> l = List.of(3, 1, 4, 2, 3, 4);

        // indexed: LinkedHashMap
        Map<Integer, Integer> ordered = new LinkedHashMap<>();
        l.forEach(i -> ordered.put(i, i));
        System.out.println(ordered);
        // keySet & values are also ordered
        System.out.println(ordered.keySet());
        System.out.println(ordered.values());

        // sorted: TreeMap
        Map<Integer, Integer> sorted = new TreeMap<>();
        l.forEach(i -> sorted.put(i, i));
        System.out.println(sorted);
        // keySet & values are also sorted
        System.out.println(sorted.keySet());
        System.out.println(sorted.values());
    }

    record Etre(int x, int y){
        double distTo(Etre o) {
            return Math.pow(x-o.x, 2) + Math.pow(y-o.y, 2);
        }
    }

    private static void heap() {
        List<Etre> etres = List.of(new Etre(1, 1), new Etre(1, 2), new Etre(1, 3));
        Etre target = new Etre(1, 2);

        // the generic type is before the method
        Comparator<Etre> comp = Comparator.<Etre>comparingDouble(e -> e.distTo(target))
                .reversed()  // reversed is a method of Comparator, so it reverses the caller
                .thenComparing(Etre::x)
                .thenComparing(Etre::y);

        // !!! there is NO SIZE limit for priority-queue (the size param is initila-size)
        Queue<Etre> q = new PriorityQueue<>(2, comp);
        q.addAll(etres);
        assertThat(q).hasSize(3);
        assertThat(q.poll()).satisfies(e -> {
            assertThat(e.x).isEqualTo(1);
            assertThat(e.y).isEqualTo(1);
        });
    }
}
