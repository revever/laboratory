package common;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

/**
 * LinkededList implements both Queue & Deque.
 *
 * 'Add' always appends to end of the list.
 * The remove/poll/pop will always remove from the first.
 *
 * Use push+pop to simulate STACK.
 *
 * Or use offerFirst/last, pollFirst/last to contral.
 */
public class Q {

    public static void main(String[] args) {
        // queue
        Queue<Integer> q = new LinkedList<>();
        for (int i = 0; i < 5; i++) {
            q.add(i);
        }
        System.out.println(q);
        q.poll();
        System.out.println(q);

        // deque
        Deque<Integer> deq = (Deque<Integer>) q;
        deq.pop();
        System.out.println(deq);

        deq.push(5);    /// should use push+pop to act as stack
        System.out.println(deq);
        deq.pop();
        System.out.println(deq);
    }

}
