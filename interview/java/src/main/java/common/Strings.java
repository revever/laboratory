package common;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Strings {

    public static void main(String[] args) {
        replace();
        join();
    }

    private static void replace() {
        // split word: \\W+
        System.out.println(Arrays.toString("I have  a tree, YOu have a   key".split("\\W+")));

        // replace all non-alpha (_ is alpha)
        System.out.println("I have  a tree, YOu have a _  key".replaceAll("\\W", ""));
        // replace all non-alpha
        System.out.println("I have  a tree, YOu have a __  key".replaceAll("\\W|_", ""));
        System.out.println("I have  a tree, YOu have a __  key".replaceAll("[\\W_]", ""));
        System.out.println("I have  a tree, YOu have a __  key".replaceAll("[^A-Za-z]", ""));

        // reverse a string
        System.out.println(new StringBuilder("abc").reverse());
    }

    private static void join() {
        String[] sa = {"hello", "world"};

        // collector
        System.out.println(Arrays.stream(sa).collect(Collectors.joining(" ")));

        // static join
        System.out.println(String.join(" ", sa));
    }
}
