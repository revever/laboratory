package common;

import java.util.Map;
import java.util.stream.Collectors;

public class Streams {

    public static void main(String[] args) {
        groupBy_count();
    }

    public static void groupBy_count() {
        String s = "hello";
        Map<Integer, Long> map = s.chars().boxed().collect(Collectors.groupingBy(i -> i, Collectors.counting()));
        System.out.println(map);
    }

}
