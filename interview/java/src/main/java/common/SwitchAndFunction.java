package common;

import java.util.List;
import java.util.function.BiPredicate;

/**
 * |
 * https://www.codingame.com/ide/puzzle/logic-gates
 */
public class SwitchAndFunction {

    /**
     * 1. interface & record are by default static.
     * 2. just create a simple interface if you don't remember the java.util.function.*.
     */
    interface Gate extends BiPredicate<Boolean, Boolean> {
    }

    /**
     * reading: https://www.baeldung.com/java-switch
     */
    private static CharSequence calc_v2(String type, List<Boolean> input1, List<Boolean> input2) {
        StringBuffer sb = new StringBuffer();
        Gate gate = switch (type) {
            case "OR" -> (b1, b2) -> b1 || b2;
            case "AND" -> (b1, b2) -> b1 && b2;
            case "XOR" -> (b1, b2) -> b1 ^ b2;
            case "NOR" -> (b1, b2) -> !(b1 || b2);
            case "NAND" -> (b1, b2) -> !(b1 && b2);
            default -> (b1, b2) -> !(b1 ^ b2);
        };
        for (int i = 0; i < input1.size(); i++) {
            sb.append(gate.test(input1.get(i), input2.get(i)) ? '-' : '_');
        }
        return sb;
    }

    private static String calc_v1(String type, List<Boolean> input1, List<Boolean> input2) {
        StringBuffer sb = new StringBuffer();
        BiPredicate<Boolean, Boolean> gate = null;
        switch (type) {
            case "OR":
                gate = (b1, b2) -> b1 || b2;
                break;
            case "AND":
                gate = (b1, b2) -> b1 && b2;
                break;
            case "XOR":
                gate = (b1, b2) -> b1 ^ b2;
                break;
            case "NOR":
                gate = (b1, b2) -> !(b1 || b2);
                break;
            case "NAND":
                gate = (b1, b2) -> !(b1 && b2);
                break;
            default:
                gate = (b1, b2) -> !(b1 ^ b2);
                break;
        }
        for (int i = 0; i < input1.size(); i++) {
            sb.append(gate.test(input1.get(i), input2.get(i)) ? '-' : '_');
        }
        return sb.toString();
    }

}
