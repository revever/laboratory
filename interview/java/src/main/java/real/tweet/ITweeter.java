package real.tweet;

import java.util.List;

public interface ITweeter {
    void addTweet(int id, double x, double y);

    List<Tweet> getClosest(int k, double x, double y);
}
