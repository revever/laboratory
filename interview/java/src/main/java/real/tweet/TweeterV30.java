package real.tweet;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 1. Min heap.
 * 2. final fields
 */
public class TweeterV30 implements ITweeter {
    private final Map<Integer, Tweet> tweetsMap = new ConcurrentHashMap<>();

    @Override
    public void addTweet(int id, double x, double y) {
        // assume there is never ID conflict
        tweetsMap.put(id, new Tweet(id, x, y));
    }

    @Override
    public List<Tweet> getClosest(int k, double x, double y) {
        Comparator<Tweet> comp = Comparator
                .<Tweet>comparingDouble(t -> t.distTo(x, y))
                .thenComparing(Tweet::x)
                .thenComparing(Tweet::y)
                .reversed();  // reverse to keep the min K tweets

        PriorityQueue<Tweet> q = new PriorityQueue<>(comp);
        for (Tweet tweet : tweetsMap.values()) {
            q.offer(tweet);
            if (q.size() > k) {
                q.poll();
            }
        }
        // the default iterator of PQ works in random order
        return new ArrayList<>(q);
    }
}


