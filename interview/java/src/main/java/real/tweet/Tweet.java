package real.tweet;

public record Tweet(int id, double x, double y) {

    public double distTo(double x, double y) {
        return Math.sqrt(Math.pow(this.x - x, 2) + Math.pow(this.y - y, 2));
    }
}
