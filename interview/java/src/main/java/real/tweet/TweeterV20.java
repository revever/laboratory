package real.tweet;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Read/write has concurrent issues.
 *
 * Several hints:
 *  CopyOnWriteArrayList is costly if too many writes.
 *  ReadWriteLock
 *  ConcurrentHashMap with weakly consistent iteration.
 */
public class TweeterV20 implements ITweeter {
    // a read-write lock may be suitable also
    private Map<Integer, Tweet> tweetsMap = new ConcurrentHashMap<>();

    @Override
    public void addTweet(int id, double x, double y) {
        // putIfAbsent or raise exception: to discuss
        this.tweetsMap.put(id, new Tweet(id, x, y));
    }

    @Override
    public List<Tweet> getClosest(int k, double x, double y) {
        Comparator<Tweet> comp = Comparator
                .<Tweet>comparingDouble(t -> t.distTo(x, y))
                .thenComparing(Tweet::x)
                .thenComparing(Tweet::y);

        return tweetsMap.values().stream()
                .sorted(comp)
                .limit(k)
                .collect(Collectors.toList());
    }
}


