package real.tweet;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 1. synchronized
 * 2. use stream.limit to get first K elements
 */
public class TweeterV11 implements ITweeter {
    private List<Tweet> tweets = new ArrayList<>();

    @Override
    public synchronized void addTweet(int id, double x, double y) {
        this.tweets.add(new Tweet(id, x, y));
    }

    @Override
    public synchronized List<Tweet> getClosest(int k, double x, double y) {
        Comparator<Tweet> comp = Comparator
                .<Tweet>comparingDouble(t -> t.distTo(x, y))
                .thenComparing(Tweet::x)
                .thenComparing(Tweet::y);

        return tweets.stream().sorted(comp)
                .limit(k)
                .collect(Collectors.toList());
    }
}


