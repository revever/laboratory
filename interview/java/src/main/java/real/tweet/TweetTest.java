package real.tweet;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TweetTest {

    public static void main(String[] args) {
        test(new TweeterV10());
        test(new TweeterV11());
        test(new TweeterV10());
        test(new TweeterV11());
        test(new TweeterV20());
        test(new TweeterV30());
    }

    private static void test(ITweeter tweeter) {
        for (int i = 0; i < 500; i++) {
            tweeter.addTweet(i, i, i);
        }

        List<Tweet> tweets = tweeter.getClosest(1, 10.1, 10.1);
        assertThat(tweets).hasSize(1);
        assertThat(tweets.get(0)).satisfies(t -> {
            assertThat(t.x()).isEqualTo(10);
            assertThat(t.y()).isEqualTo(10);
        });
    }

}
