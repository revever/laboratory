Class Tweet has id, x, y.
Design a Tweeter which has 2 methods:
- addTweet
- getTweets(int k, int x, int y): return the k first tweets most close to (x,y)