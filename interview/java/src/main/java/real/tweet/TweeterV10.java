package real.tweet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TweeterV10 implements ITweeter {
    private List<Tweet> tweets = new ArrayList<>();

    @Override
    public void addTweet(int id, double x, double y) {
        this.tweets.add(new Tweet(id, x, y));
    }

    @Override
    public List<Tweet> getClosest(int k, double x, double y) {
        Comparator<Tweet> comp = Comparator
                .<Tweet>comparingDouble(t -> t.distTo(x, y))
                .thenComparing(Tweet::x)
                .thenComparing(Tweet::y);

        Collections.sort(tweets, comp);
        return tweets.subList(0, k < tweets.size() ? k : tweets.size());
    }
}


