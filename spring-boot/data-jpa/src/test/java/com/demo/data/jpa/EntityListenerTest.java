package com.demo.data.jpa;

import com.demo.data.jpa.model.Customer;
import com.demo.data.jpa.repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Here we implement an audit feature by listening to <b>entity life-cycles</b>.
 */
@SpringBootTest
class EntityListenerTest {

    @Autowired
    private CustomerRepository repository;

    @Test
    void audit_by_listener() {
        // given
        var customer = new Customer();
        customer.setFirstName("Gilbert");
        customer.setLastName("Einstein");

        // when-then
        var saved = repository.save(customer);
        assertThat(saved.getCreatedDate()).isNotNull();
        assertThat(saved.getCreatedDate()).isEqualTo(saved.getLastModifiedDate());

        // when-then
        saved.setComment("scientist");
        var updated = repository.save(saved);
        assertThat(updated.getLastModifiedDate()).isNotEqualTo(updated.getCreatedDate());
    }

}