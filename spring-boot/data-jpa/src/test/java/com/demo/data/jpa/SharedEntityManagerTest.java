package com.demo.data.jpa;

import com.demo.data.jpa.repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * By default, the managed transactional in spring uses the shared entity-manager.
 */
@DataJpaTest
public class SharedEntityManagerTest {
    @Autowired
    private EntityManager entityManager;

    @Autowired
    private CustomerRepository repository;

    @Test
    @Transactional(Transactional.TxType.NEVER)
    void shared_entity_manager_is_NOT_used_for_not_managed_transaction() {
        final var customer = repository.findById(100L);

        assertThat(entityManager.contains(customer)).isFalse();
    }

    @Test
    @Transactional(Transactional.TxType.REQUIRED)
    void shared_entity_manager_is_used_for_managed_transaction() {
        final var customer = repository.findById(100L);

        assertThat(entityManager.contains(customer)).isTrue();
    }
}
