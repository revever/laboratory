package com.demo.data.jpa;

import com.demo.data.jpa.model.Customer;
import com.demo.data.jpa.repository.CustomerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.persistence.EntityManager;
import javax.persistence.TransactionRequiredException;
import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

/**
 * A detached entity is an entity outside any persistence context. To put it more straightforward, a detached entity
 * is a <b>POJO</b>. In order to save it to database:
 * <ul>
 *     <li>obtain a persistence context (under the hood, a db connection)</li>
 *     <li>merge the changes to persisted entity</li>
 * </ul>
 *
 * <p>3-tier design is exactly a case of this procedure, on server side:
 * <ul>
 *     <li>load the PO in a persistence context</li>
 *     <li>merge the DTO to PO (ex: with MapStruct) and then save it</li>
 * </ul>
 *
 * <b>Note</b>: the detached object remains detached (as DTO). The object returned by merge is a persisted instance.
 */
@DataJpaTest
class DetachedEntityTest {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private CustomerRepository repo;

    private Customer detached;

    @BeforeEach
    @Transactional(Transactional.TxType.NEVER)
    void before() {
        detached = repo.findById(100);
    }

    /**
     * Entity-manager needs a transaction to be available in <b>current thread</b>.
     */
    @Test
    @Transactional(Transactional.TxType.NEVER)
    void save_with_entity_manager_without_transaction_should_raise_exception() {
        assertThatExceptionOfType(TransactionRequiredException.class).isThrownBy(() -> entityManager.persist(detached));
    }
    @Test
    void save_with_entity_manager() {
        // given
        var newName = "Bill1";
        detached.setFirstName(newName);

        // when
        entityManager.persist(detached);

        // then
        assertThat(repo.findById(100).getFirstName()).isEqualTo(newName);
    }

    /**
     * No transaction needed.
     * <p>On reading the code, the spring repository actually makes a (load + merge) to save (persist/update) the entity.
     */
    @Test
    @Transactional(Transactional.TxType.NEVER)
    void save_with_repository() {
        // given
        var newName = "Bill2";
        detached.setFirstName(newName);

        // when
        var po = repo.save(detached);

        // then
        assertThat(po).isNotSameAs(detached);
        assertThat(repo.findById(100).getFirstName()).isEqualTo(newName);
    }

}
