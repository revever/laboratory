package com.demo.data.jpa;


import com.demo.data.jpa.model.Customer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

/**
 * Only managed-entity can be refreshed / removed.
 */
@DataJpaTest
class RefreshRemoveTest {

    @Autowired
    private EntityManager entityManager;

    @Test
    void refresh_detached(@Autowired EntityManagerFactory factory) {
        // given: a customer loaded from a new context (not the shared one)
        Customer customer = factory.createEntityManager().find(Customer.class, 100L);

        // then
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> entityManager.refresh(customer))
                .withMessageContaining("Entity not managed");

        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> entityManager.remove(customer))
                .withMessageContaining("Removing a detached instance");
    }

}
