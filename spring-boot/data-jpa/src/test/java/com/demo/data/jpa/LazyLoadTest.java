package com.demo.data.jpa;

import com.demo.data.jpa.model.Order;
import com.demo.data.jpa.repository.CustomerRepository;
import org.hibernate.LazyInitializationException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.transaction.Transactional;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

/**
 * By default, lazy-load only works inside a persistence-context.
 * <p>
 * Gotcha: behind the entity-manager/session, it's actually a database-connection. So obviously, no data can be loaded
 * if there is no underlying db-connection available.
 * </p>
 */
@DataJpaTest
class LazyLoadTest {

    @Autowired
    private CustomerRepository repo;

    @Test
    void lazy_load_inside_session_should_pass() {
        assertThat(repo.findById(100).getOrders()).isNotEmpty();
    }

    @Test
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    void lazy_load_inside_another_session_should_pass() {
        assertThat(repo.findById(100).getOrders()).isNotEmpty();
    }

    /**
     * We disable the default REQUIRED transaction propagation of DataJpaTest.
     */
    @Test
    @Transactional(Transactional.TxType.NEVER)
    void lazy_load_outside_session_should_throw_exception() {
        List<Order> orders = repo.findById(100).getOrders();
        assertThatExceptionOfType(LazyInitializationException.class)
                .isThrownBy(orders::size);
    }

}
