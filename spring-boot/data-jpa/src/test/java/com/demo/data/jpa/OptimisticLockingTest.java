package com.demo.data.jpa;

import com.demo.data.jpa.repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.ObjectOptimisticLockingFailureException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


/**
 * <p>Optimistic locking is often a must when 2nd-level cache is enabled, as 2nd-level cache in unaware of dirty data made
 * by other services.
 *
 * <p>The underlying update is: <code>UPDATE .... WHERE id=X and VERSION=y</code>
 */
@SpringBootTest
class OptimisticLockingTest {

    @Autowired
    private CustomerRepository repository;

    @Test
    void version_should_be_increased_on_update() {
        // given
        var bill = repository.findById(100);

        // when
        bill.getAddress().setCity("Seattle");
        var updated = repository.save(bill);

        // then
        assertEquals(bill.getVersion() + 1, updated.getVersion());
    }


    /**
     * Without the @Transactional annotation, there are 3 transactions: 1) repo.findById  2) repo.save  3) repo.save.
     * With optimisic lock, the concurrent modification is successfully detected.
     * But with pessimic lock, the concurrent modification will not be detected as the write are done in different transactions.
     */
    @Test
    void optimistic_lock_should_block_dirty_update() {
        // given
        var bill = repository.findById(100);
        bill.getAddress().setCity("NY");
        repository.save(bill);

        // when
        bill.getAddress().setCity("Washington");
        Executable saveDirtyBill = () -> repository.save(bill);

        // then
        assertThrows(ObjectOptimisticLockingFailureException.class, saveDirtyBill);
    }

}
