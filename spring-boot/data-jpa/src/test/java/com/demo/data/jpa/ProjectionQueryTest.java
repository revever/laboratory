package com.demo.data.jpa;

import com.demo.data.jpa.repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class ProjectionQueryTest {

    @Autowired
    private CustomerRepository repository;

    @Test
    void query_names_only() {
        var gates = repository.findByLastNameIgnoreCase("gaTES");

        assertThat(gates).hasSize(1);
        assertThat(gates.get(0).getLastName()).isEqualTo("Gates");
    }

}
