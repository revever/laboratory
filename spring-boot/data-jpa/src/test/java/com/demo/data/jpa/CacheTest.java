package com.demo.data.jpa;

import com.demo.data.jpa.repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;
import org.springframework.util.StringUtils;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * 1st-level cache is bound to the session for managed entities. It's like a staging area. <code>entityManager.contains(entity)</code>
 * can be used to check if an entity is cached / managed. When the session / entityManager is closed, the 1st-level cache is cleared.
 *
 * <p>2nd-level cache is application-level cache. One question is: how the 2nd-level cache behaves in a <b>distributed</b> environment.
 * Actually it depends on which cache provider is used. If we use a local EHCache, then we may have sync problem. But if
 * we use a distributed Redis cache, then we have no sync issue. Hibernate is designed to be unaware of the cache provider (cache region).
 */
@DataJpaTest
@ExtendWith(OutputCaptureExtension.class)
class CacheTest {

    @Autowired
    private CustomerRepository repository;

    @Test
    void no_second_level_cache(CapturedOutput output) {
        // given
        var queryCount = 3;

        // when
        IntStream.range(0, queryCount).forEach((i) -> repository.findById(1));

        // then
        var selectCustomer = StringUtils.countOccurrencesOf(output.getOut(), "select customer");
        assertEquals(queryCount, selectCustomer);
    }
}
