package com.demo.data.jpa;

import com.demo.data.jpa.repository.CustomerRepository;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;
import org.springframework.test.context.jdbc.Sql;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.util.StringUtils.countOccurrencesOf;

/**
 * With ENABLE_LAZY_LOAD_NO_TRANS option, we can lazy-load any time. Actually a new transaction is automatically created
 * each time the lazy-load is triggered outside a persistence-context.
 *
 * <p>
 * As ManyToOne and OneToOne are eagerly loaded, the lazily-loaded orders will eagerly load another customer instance.
 * This behavior may be <b>error-prone</b> especially when the code keeps references to customers. <br/>
 * The implementation of 2nd-level cache will not eradicate the problem. 2nd-level cache will exempt the need to load from
 * database, but the layout will still be a newly-assembled instance.
 *
 * <p>Why 2nd-level cache not actually resolves the issue?
 * <ul>
 *     <li>sometime the 2nd-level cache can't contain the whole database</li>
 *     <li>sometime there are eviction strategy</li>
 *     <li>as Hibernate is a general framework, it must assume that changes may happen at any time by other processes. So each
 *     time it lazily loads a collection, it must however make a db-query if the collections has changed (add/delete)</li>
 *     <li>as Hibernate is on lower level than application. It's not aware of the existing detached entities at app-level</li>
 * </ul>
 *
 * <p>
 * If the lazy-load is triggered inside a transaction with SUB-SELECT, this option will have no effect -> thus there is no
 * N+1 problem in this case.
 */
@DataJpaTest(properties = "spring.jpa.properties.hibernate.enable_lazy_load_no_trans=true")
@ExtendWith(OutputCaptureExtension.class)
@Log4j2
class LazyLoad_enable_lazy_load_no_trans_Test {

    @Autowired
    private CustomerRepository repo;

    @Test
    @Transactional(Transactional.TxType.NEVER)
    void lazy_load_outside_session_with_ENABLE_LAZY_LOAD_NO_TRANS_should_pass() {
        assertThat(repo.findById(100).getOrders()).isNotEmpty();
    }

    @Test
    @Transactional(Transactional.TxType.NEVER)
    void lazy_loaded_order_will_eagerly_load_another_customer_instance() {
        var customer = repo.findById(100);
        var orders = customer.getOrders();
        assertThat(orders.get(0).getCustomer()).isNotSameAs(customer);
    }

    /**
     * If lazy-load is triggered outside a session, naturally we have N+1 problem - as expected.
     * But if it is triggered inside a session, no N+1 problem with FetchMode.SUBSELECT (see Customer.orders)
     */
    @Test
    @Sql(scripts = "/more-data.sql")
    void N1_problem_should_not_happen_inside_one_transaction(CapturedOutput output) {
        // more than 1 customers
        var customers = repo.findAll();
        assertThat(customers).hasSizeGreaterThan(1);

        // only one select from table orders
        customers.forEach(customer -> log.info(customer.getOrders().size()));
        assertThat(countOccurrencesOf(output.getOut(), "select orders")).isEqualTo(1);
    }


}
