package com.demo.data.jpa;

import com.demo.data.jpa.model.Customer;
import com.demo.data.jpa.repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.TransactionSystemException;

import javax.persistence.EntityManagerFactory;
import javax.persistence.RollbackException;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

/**
 * Note: the bean validation happens on commit.
 */
@DataJpaTest
class ValidationOnCommitTest {

    @Autowired
    private CustomerRepository repository;

    @Test
    @Transactional(Transactional.TxType.NEVER)
    void validation_should_be_done_automatically_with_entity_manager(@Autowired EntityManagerFactory factory) {
        // given
        var entityManager = factory.createEntityManager();
        var transaction = entityManager.getTransaction();

        // when
        transaction.begin();
        entityManager.persist(new Customer());

        // then
        assertThatExceptionOfType(RollbackException.class)
                .isThrownBy(transaction::commit)
                .withRootCauseInstanceOf(ConstraintViolationException.class);
    }

    @Test
    @Transactional(Transactional.TxType.NEVER)
    void validation_should_be_done_automatically_with_repository() {
        var customer = new Customer();
        assertThatExceptionOfType(TransactionSystemException.class)
                .isThrownBy(() -> repository.save(customer))
                .withRootCauseInstanceOf(ConstraintViolationException.class);
    }

}
