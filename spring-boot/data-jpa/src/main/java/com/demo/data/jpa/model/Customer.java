package com.demo.data.jpa.model;

import com.demo.data.jpa.model.trait.Auditable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the Aggregate Root.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class Customer extends AbstractEntity implements Auditable {

    @Column(length = 100)
    @Length(min = 1, max = 100)
    private String firstName;

    @Column(length = 100)
    @NotNull
    private String lastName;

    @Embedded
    private Address address;

    private String comment;

    /**
     * Note the <code>ToString.Exclude</code> here. Without this annotation, the orders will be loaded systematically under
     * debug mode (by toString()) which may interfere the analysis.
     *
     * <p>The <code>new ArrayList<>()</code> initialization is mandatory if <code>orphanRemoval = true</code>
     */
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    @ToString.Exclude
    private List<Order> orders = new ArrayList<>();

    @Version
    private long version;

    private Instant createdDate;

    private Instant lastModifiedDate;

}


