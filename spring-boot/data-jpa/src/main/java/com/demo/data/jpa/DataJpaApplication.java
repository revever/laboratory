package com.demo.data.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.concurrent.Semaphore;

@SpringBootApplication
public class DataJpaApplication {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(DataJpaApplication.class);
        new Semaphore(0).acquire();
    }

}
