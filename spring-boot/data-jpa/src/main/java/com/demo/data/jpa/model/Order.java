package com.demo.data.jpa.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * 'Order' is a key-word for some databases which may lead to unexpected issues. So we
 * specify the table name to 'orders'.
 */
@Entity(name = "orders")
@Data
@EqualsAndHashCode(callSuper = false)
public class Order extends AbstractEntity {

    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Customer customer;

    private String product;

    private int quantity;

}
