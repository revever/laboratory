package com.demo.data.jpa.repository;

import com.demo.data.jpa.model.Customer;
import com.demo.data.jpa.model.projection.NamesOnly;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

	List<Customer> findByLastName(String lastName);

	Customer findById(long id);

	List<NamesOnly> findByLastNameIgnoreCase(String lastName);
}
