package com.demo.data.jpa.model.trait;

import java.time.Instant;

public interface Auditable {

    Instant getCreatedDate();

    void setCreatedDate(Instant instant);

    Instant getLastModifiedDate();

    void setLastModifiedDate(Instant instant);

}
