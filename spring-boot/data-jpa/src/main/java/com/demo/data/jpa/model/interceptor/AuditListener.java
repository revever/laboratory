package com.demo.data.jpa.model.interceptor;

import com.demo.data.jpa.model.trait.Auditable;
import lombok.extern.apachecommons.CommonsLog;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.Instant;

/**
 * For auditing, there are other solutions:
 * <ul>
 *     <li>@EnableJpaAuditing & AuditingEntityListener to audit last modification</li>
 *     <li>Hibernate Envers to audit full changelog</li>
 * </ul>
 *
 * Here we use audit just to demonstrate the usage of entity life-cycle listening.
 */
@CommonsLog
public class AuditListener {

    @PrePersist
    public void prePersist(Object target) {
        log.info("pre-persisting..." + target);

        if (target instanceof Auditable auditable) {
            var now = Instant.now();
            auditable.setCreatedDate(now);
            auditable.setLastModifiedDate(now);
        }
    }

    @PreUpdate
    public void preUpdate(Object target) {
        log.info("pre-updating..." + target);

        if (target instanceof Auditable auditable) {
            auditable.setLastModifiedDate(Instant.now());
        }
    }

}
