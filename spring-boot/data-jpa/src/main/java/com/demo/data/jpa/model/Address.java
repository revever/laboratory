package com.demo.data.jpa.model;

import lombok.Data;

import javax.persistence.Embeddable;

@Embeddable
@Data
public class Address {
    private String country;
    private String city;
}

