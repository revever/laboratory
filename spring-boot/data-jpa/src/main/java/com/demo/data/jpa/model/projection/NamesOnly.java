package com.demo.data.jpa.model.projection;

public interface NamesOnly {

    String getFirstName();

    String getLastName();

    default String getFullName() {
        return getFirstName().concat(" ").concat(getLastName());
    }
}

