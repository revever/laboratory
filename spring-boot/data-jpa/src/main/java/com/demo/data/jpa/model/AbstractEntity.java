package com.demo.data.jpa.model;

import com.demo.data.jpa.model.interceptor.AuditListener;
import lombok.Getter;

import javax.persistence.*;

@MappedSuperclass
@EntityListeners(AuditListener.class)
public class AbstractEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Getter
    protected long id;
}
