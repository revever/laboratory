-- the data.sql is loaded automatically on startup by spring-boot
-- the default naming strategy of Hibernate auto-ddl is snake case

insert into customer (id, first_name, last_name, country, version) values (100, 'Bill', 'Gates', 'US', 0);

insert into orders (id, customer_id, product, quantity) values (1000, 100, 'Office', 1);
insert into orders (id, customer_id, product, quantity) values (1001, 100, 'Windows', 1);
