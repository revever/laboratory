# Isolation

## Isolation & Transaction
When we talk about isolation, we talk about it in a transactional context. In other words, the isolation between 2+ transactions. 


## Isolation problems
- phantom read: a range select returns differents records between two runs
- non-repeatbable read: the same select on the SAME RECORD returns differents column values
- dirty read: read returns uncommitted changes of another transaction
- dirty write: 2 transactions write simultaneously the same record

## Isolation level
read-uncommitted: solves the dirty write
read-commited: solves the dirty read
repeatable-read: solved the non-repeatbable read
serializalbe: solved the phantom read



# Pessimistic & Optimistic Lock
There are several database mechanisms to implement the isolation levels, specially lock and MVCC (multiple version concurrency control).
The 'lock' here refers to functional lock - nothing to do with the lock isolation mechanism in database. However, to simplify the things, in the following discussion, we suppose the isolation mechanism is lock.


## Pessimistic 
There is an isolation level at database level, which by default is read-commited. But most databases allow to specify a lock at each sql level, ex: 'select for update'. The pessimistic is based on the database locking mechanism.

The pessimistic_read puts a shared-lock which allows concurrent read but disables concurrent write.
The pessimistic_write puts an exclusive-lock which disables concurrent read/write.

As said before, 'concurrent' refers to concurrent transactions. In case of pessimistic_write, even read transactions will be blocked until current transaction commit or rollack.

Note: a transaction may contain millions of operations/sqls. It's very possible that there are hundreds of parallel transactions at the beginning. Then suddenly one transaction requires a pessimistic_write, which will block other transaction trying to read the same data.


## Optimistic
Generally based on version or timestamp.



# Detached Entity
Entity is associated to a entity-manager (or session for Hibernate), which is actullay a database connection under the hood.

There is no way to re-attach a detached entity in JPA. In other words, there is no means to sync 
a detached entity with another entity-manager (connection). To save a detached entity, the only possibility:
<ul>
<li>load a new instance by querying the same id</li>
<li>merge the changes to the managed instance</li>
<li>return the managed instance</li>
</ul>
Surely all this should happen inside a transactin (thus by the same entity-manager). That's EXACTLY what a spring repository does.
Although the changes are saved to the database, there are multiple instances now for the same business object
(even with 2nd-level cache enabled) which is quite hard to debug in case of problem.

Why JPA can't update the existing instance?<br/>
Actually JPA sits in lower level that business logic - JPA just doesn't know about the existing instances.
No change is tracked / managed. It's unaware about what has been done with the detached object. 

## lazy-loading
When we talk about lazy-loading, we imply that the owner entity is detached. So the collections will be
loaded with a new owner instance - resulting in multiple instances for the same business object as explained 
above.

## 3-tier structure
In the service-tier, the methods are transactional. The PO are converted into DTO before returning to client side.

## 2-tier structure
Often the PO is used as DTO. In this case, the POs are often detached and referenced by IHM. When lazy-load 
happens, we will have multiple-instance problem. There are few solutions:
<ul>
<li>use DTO even in 2-tier structure: anyway, is it really a good idea to not separate PO (which is proxyed by spring)
& VO?</li>
<li>always use the firstly-loaded detached entity: actually it's not anymore a PO in this case</li>
</ul>
